//
//  DetalleMultaViewController.m
//  TableViewExample
//
//  Created by S209e19 on 18/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "DetalleMultaViewController.h"

@interface DetalleMultaViewController ()

@end

@implementation DetalleMultaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _fechaLabel.text = self.multa.fecha;
    _infraccionLabel.text = self.multa.infraccion;
    _sancionLabel.text = self.multa.sancion;
    _imageMulta.image = [UIImage imageNamed: self.multa.imageMulta];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
