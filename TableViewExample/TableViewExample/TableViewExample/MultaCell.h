//
//  MultaCell.h
//  TableViewExample
//
//  Created by S209e19 on 18/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultaCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageMulta;

@property (weak, nonatomic) IBOutlet UILabel *infraccionLabel;
@property (weak, nonatomic) IBOutlet UILabel *fechaLabel;

@end
