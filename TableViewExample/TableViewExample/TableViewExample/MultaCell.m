//
//  MultaCell.m
//  TableViewExample
//
//  Created by S209e19 on 18/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "MultaCell.h"

@implementation MultaCell

- (void)awakeFromNib {
    // Initialization code
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
