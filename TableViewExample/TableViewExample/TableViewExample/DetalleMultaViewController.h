//
//  DetalleMultaViewController.h
//  TableViewExample
//
//  Created by S209e19 on 18/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Multa.h"
@interface DetalleMultaViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *fechaLabel;
@property (weak, nonatomic) IBOutlet UILabel *infraccionLabel;
@property (weak, nonatomic) IBOutlet UILabel *sancionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageMulta;

@property (strong, nonatomic) Multa *multa;

@end
