//
//  ViewController.m
//  TableViewExample
//
//  Created by S209e19 on 18/09/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ViewController.h"
#import "MultaCell.h"
#import "Multa.h"
#import "DetalleMultaViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tableView.tableFooterView = [UIView new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.multas.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MultaCell *cell = (MultaCell *) [tableView dequeueReusableCellWithIdentifier:self.type];
    
    Multa *multa = self.multas[indexPath.row];
    
    cell.fechaLabel.text = multa.fecha;
    cell.infraccionLabel.text = multa.infraccion;
    
    if ([self.type isEqualToString:@"FotoMultaCell"]) {
        cell.imageMulta.image = [UIImage imageNamed:multa.imageMulta];
    }
    
    return cell;
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    Multa *multa = self.multas[[sender intValue]];
    DetalleMultaViewController *detalleMultaVC = [segue destinationViewController];
    detalleMultaVC.multa = multa;
    
    
}

# pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{  
    
    [self performSegueWithIdentifier:@"detalleMulta" sender:@(indexPath.row)];
    
}



















@end
